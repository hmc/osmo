# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Joe Hansen <joedalton2@yahoo.dk>, 2014-2017
# pasp <pasp@quefrency.org>, 2011
msgid ""
msgstr ""
"Project-Id-Version: Osmo\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-07-12 12:01+0200\n"
"PO-Revision-Date: 2017-09-19 09:44+0000\n"
"Last-Translator: pasp <pasp@quefrency.org>\n"
"Language-Team: Danish (http://www.transifex.com/pasp/osmo/language/da/)\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgid "Colors"
msgstr "Farver"

msgid "Color of contact tags"
msgstr "Farve på kontaktmærker"

msgid "Color of links"
msgstr "Farve på henvisninger"

msgid "Font size"
msgstr "Skriftstørrelse"

msgid "Name font size"
msgstr "Skriftstørrelse på navn"

msgid "Item font size"
msgstr "Skriftstørrelse på punkt"

msgid "Photo size"
msgstr "Billedstørrelse"

msgid "Small"
msgstr "Lille"

msgid "Medium"
msgstr "Mellem"

msgid "Large"
msgstr "Stor"

msgid "Hide group column"
msgstr "Gem gruppekolonne"

msgid "Maps provider"
msgstr "Kortleverandør"

msgid "Age"
msgstr "Alder"

msgid "Birthday date"
msgstr "Fødselsdags dato"

msgid "Zodiac sign"
msgstr "Stjernetegn"

msgid "Contacts"
msgstr "Kontakter"

msgid "Appearance"
msgstr "Udseende"

msgid "Miscellaneous"
msgstr "Diverse"

msgid "Groups"
msgstr "Grupper"

msgid "Visible columns in birthday browser"
msgstr "Synlige kolonner i fødseldagsfremviser"

msgid "Algorithm"
msgstr "Algoritme"

msgid "Hashing"
msgstr "Hashing"

msgid "Compression"
msgstr "Komprimering"

msgid "Ratio"
msgstr "Forhold"

msgid "Fast"
msgstr "Hurtig"

msgid "Good"
msgstr "God"

msgid "Best"
msgstr "Bedre"

msgid "Type"
msgstr "Type"

msgid "Category"
msgstr "Kategori"

msgid "Last changes"
msgstr "Seneste ændringer"

msgid "Created"
msgstr "Oprettet"

msgid "Remember the last selected category"
msgstr "Husk den sidst valgte kategori"

msgid "Use system format for date and time"
msgstr "Anvend systemformat for dato og tid"

msgid "Notes"
msgstr "Noter"

msgid "Encryption"
msgstr "Kryptering"

msgid "Visible columns"
msgstr "Synlige kolonner"

msgid "Categories"
msgstr "Kategorier"

msgid "Notes options"
msgstr "Noteindstillinger"

msgid "Jump to date"
msgstr "Gå til dato"

msgid "Day"
msgstr "Dag"

msgid "Month"
msgstr "Måned"

msgid "Year"
msgstr "År"

msgid "_Cancel"
msgstr "_Afbryd"

msgid "_Jump to"
msgstr "_Gå til"

msgid "Select a font..."
msgstr "Vælg en skrifttype"

msgid "None"
msgstr "Ingen"

msgid "All items"
msgstr "Alle elementer"

msgid "Question"
msgstr "Spørgsmål"

msgid "Information"
msgstr "Information"

msgid "Error"
msgstr "Fejl"

msgid "Warning"
msgstr "Advarsel"

msgid "Show hidden files"
msgstr "Vis skjulte filer"

msgid "_Save"
msgstr "_Gem"

msgid "_Open"
msgstr "_Åbn"

msgid "Selected file exist! Overwrite?"
msgstr "Den valgte fil findes! Overskriv?"

msgid "Selected contact will be removed."
msgstr "Valgt kontakt vil blive fjernet"

msgid "Are you sure?"
msgstr "Er du sikker!"

msgid "No date"
msgstr "Ingen dato"

msgid "Select photo"
msgstr "Vælg billede"

msgid "Address"
msgstr "Adresse"

msgid "Postcode"
msgstr "Postnummer"

msgid "City"
msgstr "By"

msgid "State"
msgstr "Stat"

msgid "Country"
msgstr "Land"

msgid "Edit contact"
msgstr "Rediger kontakt"

msgid "Add contact"
msgstr "Tilføj kontakt"

msgid "Personal"
msgstr "Personlig"

msgid "Group"
msgstr "Gruppe"

msgid "Home"
msgstr "Hjem"

msgid "Work"
msgstr "Arbejde"

msgid "Phones"
msgstr "Telefoner"

msgid "Internet"
msgstr "Internet"

msgid "Additional info"
msgstr "Yderligere info"

msgid "_OK"
msgstr "_O.k."

msgid "no entries"
msgstr "Ingen poster"

msgid "entry"
msgid_plural "entries"
msgstr[0] "post"
msgstr[1] "poster"

msgid "Calendar notes"
msgstr "Kalendernoter"

msgid "Search"
msgstr "Søg"

msgid "Filter"
msgstr "Filter"

msgid "Current month"
msgstr "Nuværende måned"

msgid "Selected month"
msgstr "Valgt måned"

msgid "Current year"
msgstr "Nuværende år"

msgid "Selected year"
msgstr "Valgt år"

msgid "Selected month and year"
msgstr "Valgt måned og år"

msgid "All notes"
msgstr "Alle noter"

msgid "Case sensitive"
msgstr "Skeln mellem store/små bogstaver"

msgid "Strikethrough past day notes"
msgstr "Gennemstreg forrige dags noter"

msgid "Date"
msgstr "Dato"

msgid "Note"
msgstr "Note"

msgid "_Close"
msgstr "_Luk"

msgid "Rat"
msgstr "Rotte"

msgid "Ox"
msgstr "Okse"

msgid "Tiger"
msgstr "Tiger"

msgid "Hare"
msgstr "Hare"

msgid "Dragon"
msgstr "Drage"

msgid "Snake"
msgstr "Slange"

msgid "Horse"
msgstr "Hest"

msgid "Sheep"
msgstr "Får"

msgid "Monkey"
msgstr "Abe"

msgid "Fowl"
msgstr "Høne"

msgid "Dog"
msgstr "Hund"

msgid "Pig"
msgstr "Gris"

msgid "Unknown"
msgstr "Ukendt"

msgid "Aquarius"
msgstr "Vandmand"

msgid "Pisces"
msgstr "Fisk"

msgid "Aries"
msgstr "Vædder"

msgid "Taurus"
msgstr "Tyr"

msgid "Gemini"
msgstr "Tvilling"

msgid "Cancer"
msgstr "Krebs"

msgid "Leo"
msgstr "Løve"

msgid "Virgo"
msgstr "Jomfru"

msgid "Libra"
msgstr "Vægt"

msgid "Scorpio"
msgstr "Skorpion"

msgid "Sagittarius"
msgstr "Skytte"

msgid "Capricorn"
msgstr "Stenbuk"

msgid "Osmo has to be restarted to take effect."
msgstr "Osmo skal genstarte for at træde i kraft."

msgid "Layout"
msgstr "Layout"

msgid "Vertical"
msgstr "Lodret"

msgid "Horizontal"
msgstr "Vandret"

msgid "Tabs position"
msgstr "Fanebladsposition"

msgid "Left"
msgstr "Venstre"

msgid "Right"
msgstr "Højre"

msgid "Top"
msgstr "Top"

msgid "Bottom"
msgstr "Bund"

msgid "Disable underline in links"
msgstr "Deaktiver understregning i henvisninger"

msgid "Show exit button in toolbar"
msgstr "Vis afslutningsknap i værktøjslinjen"

msgid "At least one module must be visible."
msgstr "Mindst et modul skal være synlig"

msgid "Calendar"
msgstr "Kalender"

msgid "Tasks"
msgstr "Begivenheder"

msgid "Override locale settings"
msgstr "Overskriv lokale indstillinger"

msgid "Date format"
msgstr "Datoformat"

msgid "DD-MM-YYYY"
msgstr "DD-MM-ÅÅÅÅ"

msgid "MM-DD-YYYY"
msgstr "MM-DD-ÅÅÅÅ"

msgid "YYYY-MM-DD"
msgstr "ÅÅÅÅ-MM-DD"

msgid "YYYY-DD-MM"
msgstr "ÅÅÅÅ-DD-MM"

msgid "Time format"
msgstr "Tidsformat"

msgid "hours"
msgstr "timer"

msgid "Spell checker language"
msgstr "Sprog for stavekontrol "

msgid "Enable tooltips"
msgstr "Aktiver værktøjsfif"

msgid "Remember last selected page"
msgstr "Husk sidst valgte side"

msgid "Save data after every modification"
msgstr "Gem data efter hver eneste ændring"

msgid "Web browser"
msgstr "Internetbrowser"

msgid "E-mail client"
msgstr "E-postprogram"

msgid "Sound player"
msgstr "Lydafspiller"

#, c-format
msgid "The %s pattern will be replaced with web address"
msgstr "%s mønstret vil bliver erstattet med internetadresse"

#, c-format
msgid "The %s pattern will be replaced with e-mail address"
msgstr "%s mønstret vil blive erstattet med e-postadresse"

#, c-format
msgid "The %s pattern will be replaced with sound filename"
msgstr "%s mønstret vil blive erstattet med lydfilnavnet"

msgid "Enable system tray"
msgstr "Aktiver statusfelt"

msgid "Start minimised"
msgstr "Start minimeret"

msgid "Ignore day note events"
msgstr "Ignorer dagnotebegivenheder"

msgid "General"
msgstr "Generelt"

msgid "Hide"
msgstr "Gem"

msgid "Helpers"
msgstr "Hjælpere"

msgid "System tray"
msgstr "Statusfelt"

msgid "Preferences"
msgstr "Præferencer"

msgid "Syntax"
msgstr "Syntaks"

msgid "abbreviated weekday name"
msgstr "forkortet ugedagsnavn"

msgid "full weekday name"
msgstr "fuld ugedagsnavn"

msgid "abbreviated month name"
msgstr "forkortet månedsnavn"

msgid "full month name"
msgstr "fuld månedsnavn"

msgid "day of the month"
msgstr "dag i måneden"

msgid "MM/DD/YY"
msgstr "MM/DD/ÅÅ"

msgid "day of the month without leading zeros"
msgstr "dag i måneden uden foranstillede nuller"

msgid "month"
msgid_plural "months"
msgstr[0] "måned"
msgstr[1] "måneder"

msgid "year without century"
msgstr "år uden århundrede"

msgid "year with century"
msgstr "år med århundrede"

msgid "Date header format"
msgstr "Teksthovedformat for hoved"

msgid "Set default format"
msgstr "Angiv standardformat"

msgid "Date format syntax"
msgstr "Formatsyntaks for dato"

msgid "Day note marker"
msgstr "Markør af dagnote"

msgid "Event marker"
msgstr "Begivenhedsmarkør"

msgid "Circle"
msgstr "Cirkel"

msgid "Ellipse"
msgstr "Ellipse"

msgid "Wave"
msgstr "Bølge"

msgid "Current day marker"
msgstr "Nuværende dagmarkør"

msgid "Arrow"
msgstr "Pil"

msgid "Free-hand circle"
msgstr "Frihåndscirkel"

msgid "Background color"
msgstr "Baggrundsfarve"

msgid "Header background color"
msgstr "Baggrundsfarve for teksthoveder"

msgid "Header foreground color"
msgstr "Forgrundsfarve for teksthoveder"

msgid "Weekend days color"
msgstr "Farve for weekenddage"

msgid "Day color"
msgstr "Dag-farve"

msgid "Previous/Next month's day color"
msgstr "Forrige/næste måneds dag-farve"

msgid "Cursor color"
msgstr "Markørfarve"

msgid "Event marker color"
msgstr "Farve på begivenhedsmarkør "

msgid "Current day marker color"
msgstr "Farve på markør for nuværende dag"

msgid "Birthday marker color"
msgstr "Farve på fødselsdagsmarkør"

msgid "Date font"
msgstr "Datoskrifttype"

msgid "Calendar font"
msgstr "Kalenderskrifttype"

msgid "Note font"
msgstr "Noteskrifttype"

msgid "Enable block cursor"
msgstr "Aktiver blokmarkør"

msgid "Cursor thickness"
msgstr "Tykkelse på markør"

msgid "Thin"
msgstr "Tynd"

msgid "Thick"
msgstr "Tyk"

msgid "Edit category"
msgstr "Rediger kategori"

msgid "Color"
msgstr "Farve"

msgid "Name"
msgstr "Navn"

msgid "Select ICS file"
msgstr "Vælg ICS-fil"

msgid "All Files"
msgstr "Alle filer"

msgid "Calendar files (*.ics)"
msgstr "Kalenderfiler (*.ics)"

msgid "Modify ICAL parameters"
msgstr "Ændr ICAL-parametre"

msgid "Filename"
msgstr "Filnavn"

msgid "Options"
msgstr "Indstillinger"

msgid "Disabled"
msgstr "Deaktiveret"

msgid "Valid"
msgstr "Gyldig"

msgid "Description"
msgstr "Beskrivelse"

msgid "Full date"
msgstr "Fuld dato"

msgid "Mark"
msgstr "Marker"

msgid "Week start on Monday"
msgstr "Ugen starter om mandagen"

msgid "Show day names"
msgstr "Vis navne på dagene"

msgid "No month change"
msgstr "Skift ikke måned"

msgid "Show week numbers"
msgstr "Vis ugenumre"

msgid "Simple view in full-year calendar"
msgstr "Simpel visning i kalender for hele året"

msgid "Enable auxilary calendars"
msgstr "Aktiver ekstra kalendere"

msgid "Ascending sorting in day notes browser"
msgstr "Stigende sortering i fremviseren for i dag-noter"

msgid "Enable spell checker in day notes"
msgstr "Aktiver stavekontrol  i dagsnoter"

msgid "Current time"
msgstr "Klokken er nu"

msgid "Show seconds"
msgstr "Vis sekunder"

msgid "Day number"
msgstr "Dagens nummer"

msgid "Today distance"
msgstr "Dagens afstand"

msgid "Marked days"
msgstr "Markerede dage"

msgid "Week number"
msgstr "Ugenummer"

msgid "Weekend days"
msgstr "Weekenddage"

msgid "Day category"
msgstr "Dagens kategori"

msgid "Moon phase"
msgstr "Månefase"

msgid "Day notes"
msgstr "Dagens noter"

msgid "Day categories"
msgstr "Dagkategorier"

msgid "iCalendar files"
msgstr "iCalender-filer"

msgid "Show in day info panel"
msgstr "Vis i infopanel for dag"

msgid "Tasks list"
msgstr "Liste over begivenheder"

#. TRANSLATORS: "No." is an abbreviation of word "number"
msgid "No."
msgstr "Nej."

msgid "Summary"
msgstr "Opsummering"

msgid "Due date"
msgstr "Forfaldsdato"

msgid "Priority"
msgstr "Prioritet"

msgid "Error printing"
msgstr "Udskrivningsfejl"

msgid "No birthdays defined"
msgstr "Ingen fødselsdage defineret"

msgid "Birthdays list"
msgstr "Fødselsdags liste"

msgid "Days to birthday"
msgstr "Dage til fødselsdag"

msgid "today"
msgstr "i dag"

msgid "Select CSV file"
msgstr "Vælg CSV-fil"

msgid "CSV (comma-separated values) files (*.csv)"
msgstr "CSV-filer (kommasepareret) (*.csv)"

msgid "Please select file first."
msgstr "Vælg venligst en fil først"

msgid "Import contacts"
msgstr "Importer kontakter"

msgid "Import type"
msgstr "Importtype"

msgid "File"
msgstr "Fil"

msgid "Input filename"
msgstr "Inddatafilnavn"

msgid "Import"
msgstr "Importer"

msgid "Nothing to import."
msgstr "Intet at importere."

msgid "contact added"
msgid_plural "contacts added"
msgstr[0] "kontakt tilføjet"
msgstr[1] "kontakter tilføjet"

msgid "of"
msgstr "af"

msgid "Record"
msgstr "Fortegnelse"

msgid "Number fields per record"
msgstr "Antal felter per fortegnelse"

msgid "Use first record as header"
msgstr "Anvend første fortegnelse som hoved"

msgid "Field type"
msgstr "Felttype"

msgid "Value"
msgstr "Værdi"

msgid "No records found in selected file."
msgstr "Ingen fortegnelser fundet i valgt fil."

#, c-format
msgid "Cannot read file."
msgstr "Kan ikke læse fil"

msgid "Cannot open file."
msgstr "Kan ikke åbne fil"

msgid "Counter"
msgstr "Tæller"

msgid "Show calendar"
msgstr "Vis kalender"

msgid "Show tasks"
msgstr "Vis begivenheder"

msgid "Show contacts"
msgstr "Vis kontakter"

msgid "Show notes"
msgstr "Vis noter"

msgid "Show options"
msgstr "Vis indstillinger"

msgid "Quit"
msgstr "Afslut"

msgid "Show small calendar window"
msgstr "Vis lille kalendervindue"

msgid "Check for events since last run"
msgstr "Kontroller for begivenheder siden sidste kørsel"

msgid "Number of days to check forward for events (default: 0)"
msgstr "Antal dage til kontrol for begivenheder (standard: 0)"

msgid "Set absolute path for settings and data files"
msgstr ""

msgid "Match contacts with given string"
msgstr "Match kontakter med angiven streng"

msgid "HTML renderer stylesheet file"
msgstr ""

msgid ""
"The user config path option is deprecated. Use XDG environment variables "
"instead."
msgstr ""
"Brugerindstillingen for konfigurationsstien er forældet. Brug XDG-"
"miljøvariabler i stedet for."

msgid "ERROR: Cannot create config files"
msgstr "FEJL: Kan ikke oprette konfigurationsfiler"

msgid "Another copy of OSMO is already running."
msgstr "En anden kopi af OSMO kører allerede"

msgid ""
"Simultaneously use two or more copies of OSMO can be a cause of data loss."
msgstr ""
"Anvendelse af to eller flere versioner af OSMO på samme tid kan medføre "
"datatab"

msgid "Do you really want to continue?"
msgstr "Vil du virkelig fortsætte"

#, fuzzy, c-format
msgid ""
"The old Osmo config directory %s is found. The config files and data will be "
"copied to XDG compatible directories. Delete the old config directory after "
"a successful migration.\n"
msgstr ""
"Den gamle konfigurationsmappe til Osmo %s blev fundet. Konfigurationsfilerne "
"og dataene vil blive kopieret til XDG-kompatible mapper. Slet den gamle "
"konfigurationsmappe efter en succesfuld migrering."

#, c-format
msgid ""
"OSMO v%s (handy personal organizer)\n"
"Copyright (c) 2007-2017 Tomasz Maka <pasp@users.sourceforge.net>\n"
"\n"
"Configuration directory = %s\n"
"Data directory          = %s"
msgstr ""

#, c-format
msgid "Failed to parse the command line arguments %s.\n"
msgstr ""

#, c-format
msgid "Stylesheet file is too large. Max size is %d.\n"
msgstr ""

msgid "Low"
msgstr "Lav"

msgid "High"
msgstr "Høj"

msgid "Done!"
msgstr "Færdig!"

msgid "contact exported"
msgid_plural "contacts exported"
msgstr[0] "kontakt eksporteret"
msgstr[1] "kontakter eksporteret"

msgid "Cannot create file."
msgstr "Kan ikke oprette filer"

msgid "Select output file"
msgstr "Vælg uddatafil"

msgid "Export contacts"
msgstr "Eksporter kontakter"

msgid "Output format"
msgstr "Uddataformat"

msgid "Add header"
msgstr "Tilføj hoved"

msgid "Fields to export"
msgstr "Felter der skal eksporteres"

msgid "All"
msgstr "Alle"

msgid "Invert"
msgstr "Inverter"

msgid "Select fields"
msgstr "Vælg felter"

msgid "Output filename"
msgstr "Filnavn for uddata"

msgid "Export"
msgstr "Eksporter"

msgid "Color of items that are due today"
msgstr "Farve på element der er forfalden i dag"

msgid "Color of items that are due in the next 7 days"
msgstr "Farve på element der forfalder i de kommende 7 dage"

msgid "Color of items that are past due"
msgstr "Farve på elementer der er gået over tid"

msgid "Task info font"
msgstr "Skrifttype for info om begivenheder"

msgid "Show in bold tasks with high priority"
msgstr "Vis med fed skrift begivenheder med høj prioritet"

msgid "Hide completed tasks"
msgstr "Gem afsluttede begivenheder"

msgid "Delete completed tasks without confirmation"
msgstr "Slet afsluttede begivenheder uden bekræftelse"

msgid "Add new task when double clicked on tasks list"
msgstr "Tilføj ny begivenhed når der dobbeltklikkes på begivenhedslisten"

msgid "Postpone time"
msgstr "Udsæt tid"

msgid "minutes"
msgstr "minutter"

msgid "0 for disable"
msgstr "0 for at deaktivere"

msgid "Repeat sound alarm"
msgstr "Gentag lydalarm"

msgid "times"
msgstr "tider"

msgid "Global notification command"
msgstr "Global påmindelseskommando"

msgid "Tasks options"
msgstr "Indstillinger for begivenheder"

msgid "Reminder options"
msgstr "Indstillinger for påmindelse"

msgid "Insert timeline"
msgstr "Indsæt tidslinje"

msgid "Timeline"
msgstr "Tidslinje"

msgid "From (hour)"
msgstr "Fra (time)"

msgid "Step (minutes)"
msgstr "Skridt (minutter)"

msgid "To (hour)"
msgstr "Til (time)"

msgid "_Paste"
msgstr "_Indsæt"

msgid "Please select at least one day when recurrency is enabled."
msgstr "Vælg venligst mindst en dag når tilbagevendende er aktiveret"

msgid "Set time"
msgstr "Vælg tidspunkt"

msgid "Hour"
msgstr "Time"

msgid "Minute"
msgstr "Minut"

msgid "Today"
msgstr "I dag"

msgid "Tomorrow"
msgstr "i morgen"

msgid "Selected task will be removed."
msgstr "Valgt begivenhed vil blive fjernet"

msgid "Select date and time"
msgstr "Vælg dato og tid"

msgid "Enable sound notification"
msgstr "Aktiver lydpåmindelse"

msgid "Enable notification dialog"
msgstr "Aktiver påmindelsesdialog"

msgid "Alarm warning"
msgstr "Alarmadvarsel"

msgid "Days"
msgstr "Dage"

msgid "Hours"
msgstr "Timer"

msgid "Minutes"
msgstr "Minutter"

msgid "Alarm command"
msgstr "Alarmkommando"

msgid "Date period"
msgstr "Datoperiode"

msgid "Months"
msgstr "Måneder"

msgid "Repeat"
msgstr "Gentag"

msgid "Repeat in the following days"
msgstr "Gentag i de følgende dage"

msgid "Time period"
msgstr "Tidsperiode"

msgid "Start"
msgstr "Start"

msgid "End"
msgstr "Slut"

msgid "Interval"
msgstr "Interval"

msgid "Ignore alarm when task expired offline"
msgstr "Ignorer alarm hvis begivenheden udløb frakoblet"

msgid "Recurrent task"
msgstr "Tilbagevendende begivenhed"

msgid "Enable"
msgstr "Aktiver"

msgid "Edit task"
msgstr "Rediger begivenhed"

msgid "Add task"
msgstr "Tilføj begivenhed"

msgid "Basic"
msgstr "Grundlæggende"

msgid "Advanced"
msgstr "Avanceret"

msgid "New Moon"
msgstr "Nymåne"

msgid "Waxing Crescent Moon"
msgstr "Tiltagende halvmåne"

msgid "Quarter Moon"
msgstr "1. kvarter"

msgid "Waxing Gibbous Moon"
msgstr "Tiltagende gibbousmåne"

msgid "Full Moon"
msgstr "Fuldmåne"

msgid "Waning Gibbous Moon"
msgstr "Aftagende gibbousmåne"

msgid "Last Quarter Moon"
msgstr "3. kvarter"

msgid "Waning Crescent Moon"
msgstr "Aftagende halvmåne"

msgid "Cannot create backup!"
msgstr "Kan ikke oprette kopi!"

msgid "Save backup"
msgstr "Gem en kopi"

msgid "Password protection"
msgstr "Beskyttelse med adgangskode"

msgid "Enter password"
msgstr "Indtast adgangskode"

msgid "Re-enter password"
msgstr "Indtast adgangskoden igen"

msgid "Please enter the password"
msgstr "Indtast venligst adgangskoden"

msgid "Passwords do not match!"
msgstr "Adgangskoderne er ikke ens!"

msgid "Backup file saved successfully!"
msgstr "Kopifilen er gemt korrekt"

msgid "Open backup file"
msgstr "Åbn kopifil"

msgid "Osmo backup files (*.bck)"
msgstr "Osmo-kopifiler (*.bck)"

msgid "This is not Osmo backup file"
msgstr "Dette er ikke en Osmo-kopifil"

msgid "Incorrect password!"
msgstr "Forkert adgangskode!"

msgid "All your data will be replaced with backup file content."
msgstr "Alle dine data vil blive erstattet af kopifilens indhold"

msgid "Osmo has to be restarted now..."
msgstr "Osmo skal nu genstartes"

msgid "Please select address"
msgstr "Vælg venligst adresse"

msgid "First name"
msgstr "Fornavn"

msgid "Last name"
msgstr "Efternavn"

msgid "Second name"
msgstr "Efternavn"

msgid "Nickname"
msgstr "Kælenavn"

msgid "Tags"
msgstr "Mærkater"

msgid "Name day date"
msgstr "Navnedagsdato"

msgid "Home address"
msgstr "Hjemadresse"

msgid "Home postcode"
msgstr "Hjem-postnummer"

msgid "Home city"
msgstr "Hjemby"

msgid "Home state"
msgstr "Hjemstat"

msgid "Home country"
msgstr "Hjemland"

msgid "Organization"
msgstr "Organisation"

msgid "Department"
msgstr "Avdeling"

msgid "Work address"
msgstr "Arbejdsadresse"

msgid "Work postcode"
msgstr "Arbejdspostnummer"

msgid "Work city"
msgstr "Arbejdsby"

msgid "Work state"
msgstr "Arbejdsstat"

msgid "Work country"
msgstr "Arbejdsland"

msgid "Fax"
msgstr "Fax"

msgid "Home phone"
msgstr "Hjemmetelefon"

msgid "Home phone 2"
msgstr "Hjemmetelefon 2"

msgid "Home phone 3"
msgstr "Hjemmetelefon 3"

msgid "Home phone 4"
msgstr "Hjemmetelefon 4"

msgid "Work phone"
msgstr "Arbejdstelefon"

msgid "Work phone 2"
msgstr "Arbejdstelefon 2"

msgid "Work phone 3"
msgstr "Arbejdstelefon 3"

msgid "Work phone 4"
msgstr "Arbejdstelefon 4"

msgid "Cell phone"
msgstr "Mobiltelefon"

msgid "Cell phone 2"
msgstr "Mobiltelefon 2"

msgid "Cell phone 3"
msgstr "Mobiltelefon 3"

msgid "Cell phone 4"
msgstr "Mobiltelefon 4"

msgid "E-Mail"
msgstr "E-post"

msgid "E-Mail 2"
msgstr "E-post 2"

msgid "E-Mail 3"
msgstr "E-post 3"

msgid "E-Mail 4"
msgstr "E-post 4"

msgid "WWW"
msgstr "WWW"

msgid "WWW 2"
msgstr "WWW 2"

msgid "WWW 3"
msgstr "WWW 3"

msgid "WWW 4"
msgstr "WWW 4"

msgid "IM Gadu-Gadu"
msgstr "IM Gadu Gadu"

msgid "IM Yahoo"
msgstr "IM Yahoo"

msgid "IM MSN"
msgstr "IM MSN"

msgid "IM ICQ"
msgstr "IM ICQ"

msgid "IM AOL"
msgstr "IM AOL"

msgid "IM Jabber"
msgstr "IM jabber"

msgid "IM Skype"
msgstr "IM Skype"

msgid "IM Tlen"
msgstr "IM Tlen"

msgid "Blog"
msgstr "Blog"

msgid "Photo"
msgstr "Billede"

msgid "New contact"
msgstr "Ny kontakt"

msgid "Remove contact"
msgstr "Fjern kontakt"

msgid "Show birthdays"
msgstr "Vis fødselsdage"

msgid "Show contact location on the map"
msgstr "Vis kontaktlokation på kortet"

msgid "Backup data"
msgstr "Lav sikkerhedskopi af data"

msgid "Restore data"
msgstr "Gendan data"

msgid "About"
msgstr "Om"

msgid "Clear"
msgstr "Ryd"

msgid "First Name"
msgstr "Fornavn"

msgid "Last Name"
msgstr "Efternavn"

msgid "All fields"
msgstr "Alle felter"

msgid "Contact details"
msgstr "Kontaktdetaljer"

msgid "Close contact panel"
msgstr "Luk kontaktpanel"

msgid "The note has changed."
msgstr "Noten er blevet ændret."

msgid "Do you want to save it?"
msgstr "Ønsker du at gemme den"

msgid "No URLs found in the selection..."
msgstr "Ingen adresser fundet i markeringen ..."

msgid "URLs will be opened in the default browser."
msgstr "Adresser vil blive åbnet i standardbrowseren."

msgid "Do you want to continue?"
msgstr "Ønsker du at fortsætte?"

msgid "Too many links selected!"
msgstr "For mange valgte henvisninger!"

msgid "links opened"
msgstr "henvisninger blev åbnet"

msgid "Words"
msgstr "Ord"

msgid "Lines"
msgstr "Linjer"

msgid "Characters"
msgstr "Bogstaver"

msgid "White characters"
msgstr "Hvide bogstaver"

msgid "Bytes"
msgstr "Byte"

msgid "Document"
msgstr "Dokument"

msgid "Selection"
msgstr "Markering"

msgid "Modified"
msgstr "Ændret"

msgid "New note"
msgstr "Ny note"

msgid "Edit note"
msgstr "Rediger note"

msgid "Delete note"
msgstr "Slet note"

msgid "Save note"
msgstr "Gem note"

msgid "Find"
msgstr "Find"

msgid "Toggle spell checker"
msgstr "Slå stavekontrol til/fra"

msgid "Bold"
msgstr "Fed"

msgid "Italic"
msgstr "Kursiv"

msgid "Underline"
msgstr "Understreg"

msgid "Strikethrough"
msgstr "Gennemstreg"

msgid "Highlight"
msgstr "Fremhæv"

msgid "Clear attributes"
msgstr "Fjern attributter"

msgid "Open URL links"
msgstr "Åbn adressehenvisninger"

msgid "Insert current date and time"
msgstr "Indsæt nuværende dato og tid "

msgid "Insert separator"
msgstr "Indsæt seperator"

msgid "Statistics"
msgstr "Statistik"

msgid "Close editor"
msgstr "Luk redigeringsprogram"

msgid "Note name"
msgstr "Notenavn"

msgid "Read-only"
msgstr "Skrivebeskyttet"

msgid "Line"
msgstr "Linje"

msgid "Column"
msgstr "Kolonne"

msgid "case sensitive"
msgstr "Forskel på store og små bogstaver"

msgid "Close find entry"
msgstr "Luk find-indgang"

msgid "Edit entry"
msgstr "Rediger indgang"

msgid "Remember cursor position"
msgstr "Husk markørposition"

msgid "Selected note will be removed."
msgstr "Valgt note vil blive fjernet"

msgid "No further data recovery will be possible."
msgstr "Det vil ikke være muligt at genoprette yderligere data"

msgid "Add note"
msgstr "Tilføj note"

msgid "Plain"
msgstr "Almindelig"

msgid "Encrypted"
msgstr "Krypteret"

msgid "Note type"
msgstr "Notetype"

msgid "Cannot open the note."
msgstr "Kan ikke åbne noten"

msgid "encryption support is disabled"
msgstr "Understøttelse for kryptering er deaktiveret"

msgid "Authorization"
msgstr "Godkendelse"

msgid "year"
msgid_plural "years"
msgstr[0] "år"
msgstr[1] "år"

msgid "day"
msgid_plural "days"
msgstr[0] "dag"
msgstr[1] "dage"

msgid "hour"
msgid_plural "hours"
msgstr[0] "time"
msgstr[1] "timer"

msgid "minute"
msgid_plural "minutes"
msgstr[0] "minut"
msgstr[1] "minutter"

msgid "second"
msgid_plural "seconds"
msgstr[0] "sekund"
msgstr[1] "sekunder"

msgid "or"
msgstr "eller"

msgid "rounded down"
msgstr "afrundet nedad"

msgid "week"
msgid_plural "weeks"
msgstr[0] "uge"
msgstr[1] "uger"

msgid "working day"
msgid_plural "working days"
msgstr[0] "arbejdsdag"
msgstr[1] "arbejdsdage"

msgid "time is ignored"
msgstr "tiden ignoreres"

msgid "weekend day"
msgid_plural "weekend days"
msgstr[0] "weekenddag"
msgstr[1] "weekenddage"

msgid "This calculator only supports dates after year 1."
msgstr "Denne lommeregner fungerer kun med datoer efter år 1."

msgid "weekend day ignored"
msgid_plural "weekend days ignored"
msgstr[0] "weekenddag ignoreret"
msgstr[1] "weekenddage ignoreret"

msgid "Date calculator"
msgstr "Dato-udregner"

msgid "Current date"
msgstr "Nuværende dato"

msgid "Set current date"
msgstr "Vælg nuværende dato"

msgid "Second"
msgstr "Sekund"

msgid "Set current time"
msgstr "Vælg nuværende klokkeslæt"

msgid "Reset time"
msgstr "Nulstil tiden"

msgid "First date and time"
msgstr "Første dato og tid"

msgid "Second date and time"
msgstr "Anden dato og tid"

msgid "Alternative time units"
msgstr "Alternative tidsenheder"

msgid "Result"
msgstr "Resultat"

msgid "Duration between two dates"
msgstr "Varighed mellem to datoer"

msgid "Operation"
msgstr "Handling"

msgid "add"
msgstr "læg til"

msgid "subtract"
msgstr "træk fra"

msgid "Date and time to add or subtract from"
msgstr "Dato og tid hvorfra der skal lægges til eller trækkes fra"

msgid "Years"
msgstr "År"

msgid "Weeks"
msgstr "Uger"

msgid "Seconds"
msgstr "Sekunder"

msgid "Ignore weekend days"
msgstr "Ignorer weekenddage"

msgid "Reset fields"
msgstr "Ryd felter"

msgid "Time to add or subtract"
msgstr "Tid der skal lægges til eller trækkes fra"

msgid "Add to or subtract from a date"
msgstr "Læg til eller træk fra en dato"

msgid "Cannot perform selected operation."
msgstr "Kan ikke udføre valgt handling"

msgid "Task has been modified or removed."
msgstr "Begivenheden er blevet ændret eller fjernet"

msgid "Remind me later"
msgstr "Påmind mig senere"

msgid "Done"
msgstr "Færdig"

msgid "Show task"
msgstr "Vis begivenhed"

msgid "Alarm warning!"
msgstr "Alarmadvarsel!"

msgid "postponed"
msgstr "udsat"

msgid "Day note"
msgstr "Dag note"

msgid "Events"
msgstr "Begivenheder"

msgid "Event"
msgstr "Hændelse"

msgid "Task"
msgstr "Begivenhed"

msgid "Birthday"
msgstr "Fødselsdag"

msgid "Show Osmo"
msgstr "Fra Osmo"

msgid "event"
msgid_plural "events"
msgstr[0] "begivenhed"
msgstr[1] "begivenheder"

msgid "No valid calendars defined"
msgstr "Ingen gyldig kalender defineret"

msgid "iCalendar events"
msgstr "iCalendar-begivenheder"

msgid "Time"
msgstr "Tid"

msgid "The list will be cleared and all entries will be lost."
msgstr "Denne liste vil blive ryddet og alt indhold vil være tabt."

msgid "event exported"
msgid_plural "events exported"
msgstr[0] "begivenhed eksporteret"
msgstr[1] "begivenheder eksporteret"

msgid "iCalendar export"
msgstr "iCalendar-eksport"

msgid "Day Selector"
msgstr "Dagvælger"

msgid "Use date period"
msgstr "Brug datoperiode"

msgid "_Add"
msgstr "_Tilføj"

msgid "_Clear"
msgstr "_Ryd"

msgid "_Remove"
msgstr "_Fjern"

msgid "No tasks with defined date found."
msgstr "Ingen begivenheder fundet med den definerede dato"

msgid "Save tasks"
msgstr "Gem begivenheder"

msgid "tasks exported"
msgstr "begivenhed eksporteret"

msgid "was born"
msgstr "blev født"

msgid "year old"
msgid_plural "years old"
msgstr[0] "år gammel"
msgstr[1] "år gammel"

msgid "day till end of year"
msgid_plural "days till end of year"
msgstr[0] "dag til årets udløb"
msgstr[1] "dage til årets udløb"

msgid "the last day of the year"
msgstr "årets sidste dag"

msgid "Day tasks"
msgstr "Dagens begivenheder"

msgid "Selected day note will be removed."
msgstr "Valgte dagnote vil blive fjernet"

msgid "Continue?"
msgstr "Fortsæt?"

msgid "Previous year"
msgstr "Foregående år"

msgid "Previous month"
msgstr "Foregående måned"

msgid "Previous day"
msgstr "Forrige dag"

msgid "Next day"
msgstr "Næste dag"

msgid "Next month"
msgstr "Næste måned"

msgid "Next year"
msgstr "Næste år"

msgid "Full-year calendar"
msgstr "Kalender for hele året"

msgid "Print calendar"
msgstr "Udskriv kalender"

msgid "Toggle day note panel"
msgstr "Slå dagens notepanel til/fra"

msgid "Select day color"
msgstr "Vælg dagens farve"

msgid "Browse notes"
msgstr "Gennemse noter"

msgid "Browse iCal events"
msgstr "Gennemse iCal-begivenheder"

msgid "Export to iCal file"
msgstr "Eksporter til iCal-fil"

msgid "Previous and next month"
msgstr "Foregående og næste måned"

msgid "Close note panel"
msgstr "Luk notepanel"

msgid "Clear text"
msgstr "Ryd tekst"

msgid "Info"
msgstr "Info"

msgid "Select color"
msgstr "Vælg farve"

msgid "Name day"
msgstr "Dagens navn"

msgid "Printing properties"
msgstr "Udskrivningsegenskaber"

msgid "Month name"
msgstr "Månedens navn"

msgid "Day name"
msgstr "Dagens navn"

msgid "Fonts"
msgstr "Skrifttyper"

msgid "Birthdays"
msgstr "Fødselsdag"

msgid "Namedays"
msgstr "Navnedage"

msgid "Visible events"
msgstr "Synlige begivenheder"

msgid "Padding"
msgstr "Polstring"

msgid "Event maximum length"
msgstr "Maksimumlængde for begivenhed"

msgid "Page orientation:"
msgstr "Sideretning:"

msgid "Portrait"
msgstr "Portræt"

msgid "Landscape"
msgstr "Landskab"

msgid "Disable background colors"
msgstr "Deaktiver baggrundsfarver"

msgid "Leap year"
msgstr "Skudår"

msgid "Yes"
msgstr "Ja"

msgid "No"
msgstr "Nej"

msgid "Chinese year animal"
msgstr "Dyr for kinesisk år"

msgid "Number of days"
msgstr "Antal dage"

msgid "Number of weeks"
msgstr "Antal uger"

msgid "Number of weekend days"
msgstr "Antal weekenddage"

msgid "Year info"
msgstr "År-info"

msgid "Alternative view"
msgstr "Alternativ visning"

msgid "Started"
msgstr "Begyndt"

msgid "Finished"
msgstr "Færdig"

msgid "New task"
msgstr "Ny begivenhed"

msgid "Remove task"
msgstr "Fjern begivenhed"

msgid "Change due date to previous date"
msgstr "Ændr forfaldsdato til foregående dato"

msgid "Change due date to next date"
msgstr "Ændr forfaldsdato til næste dato"

msgid "Export tasks"
msgstr "Eksporter begivenheder"

msgid "Print visible tasks list"
msgstr "Udskriv den synlige liste over begivenheder"

msgid "Task details"
msgstr "Detaljer for begivenhed"

msgid "Close description panel"
msgstr "Luk beskrivelsespanel"

msgid "Ctrl+PageUp"
msgstr "Ctrl+SideOp"

msgid "switch to previous tab"
msgstr "skift til tidligere faneblad"

msgid "Ctrl+PageDn"
msgstr "Ctrl+SideNed"

msgid "switch to next tab"
msgstr "skift til næste faneblad"

msgid "switch to selected page"
msgstr "skift til den valgte side"

msgid "show options window"
msgstr "vis vinduet indstillinger"

msgid "show about window"
msgstr "vis vinduet om"

msgid "toggle fullscreen mode"
msgstr "skift fuldskærmstilstand"

msgid "PageUp/PageDn"
msgstr "SideOp/SideNed"

msgid "switch page in options and about tab"
msgstr "skift side i indstillinger og om fanebladet "

msgid "exit"
msgstr "luk"

msgid "Space"
msgstr "Mellemrum"

msgid "select current date"
msgstr "vælg nuværende dato"

msgid "Ctrl+Space"
msgstr "Ctrl+Mellemrum"

msgid "toggle personal data visibility"
msgstr "slå visning af personlig data fra/til"

msgid "Arrows"
msgstr "Pile"

msgid "change day"
msgstr "skift dag"

msgid "Ctrl+Up/Down"
msgstr "Ctrl+op/ned"

msgid "scroll the contents in the day info panel"
msgstr "Rul indholdet i infopanelet for dagen"

msgid "change month"
msgstr "skift måned"

msgid "Home/End"
msgstr "Hjem/slut"

msgid "change year"
msgstr "skift år"

msgid "toggle calendars for the previous and next month"
msgstr "skift mellem kalender for den tidligere og næste måned"

msgid "day notes browser"
msgstr "fremviser for dagnoter"

msgid "assign background color to day note"
msgstr "tilføj baggrundsfarve til daglig note"

msgid "date calculator"
msgstr "datoudregner"

msgid "show full-year calendar"
msgstr "Vis kalender for hele året"

msgid "jump to date"
msgstr "spring til dato"

msgid "Delete"
msgstr "Slet"

msgid "remove day note"
msgstr "fjern daglig note"

msgid "Alt+Arrows"
msgstr "Alt+pile"

msgid "Esc"
msgstr "Esc"

msgid "close editor"
msgstr "luk redigeringsprogram"

msgid "toggle bold"
msgstr "slå fed til/fra"

msgid "toggle italic"
msgstr "slå kursiv til/fra"

msgid "toggle underline"
msgstr "slå understregning til/fra"

msgid "toggle strikethrough"
msgstr "slå gennemstreget til/fra"

msgid "toggle highlight"
msgstr "slå fremhævet til/fra"

msgid "Arrows Up/Down"
msgstr "Pile op/ned"

msgid "toggle alternative view"
msgstr "slå alternativ visning til/fra"

msgid "year info"
msgstr "år-info"

msgid "set current year"
msgstr "bestem nuværende år"

msgid "close full-year calendar"
msgstr "luk kalender for hele året"

msgid "Alt+a, Insert"
msgstr "Alt+a, indsæt"

msgid "add task"
msgstr "tilføj begivenhed"

msgid "Alt+e, Ctrl+Enter"
msgstr "Alt+e, Ctrl+Retur"

msgid "edit task"
msgstr "rediger begivenhed"

msgid "Alt+r, Delete"
msgstr "Alt+r, Slet"

msgid "remove task"
msgstr "fjern begivenhed"

msgid "toggle hidden tasks"
msgstr "slå skjulte begivenheder til/fra"

msgid "activate search field"
msgstr "aktiver søgefelt"

msgid "Left, Right"
msgstr "Venstre, højre"

msgid "change category filter"
msgstr "skift kategorifilter"

msgid "close task info panel"
msgstr "luk informationspanel for begivenhed"

msgid "Insert"
msgstr "Indsæt"

msgid "add contact"
msgstr "tilføj kontakt"

msgid "Ctrl+Enter"
msgstr "Ctrl+Retur"

msgid "edit contact"
msgstr "rediger kontakt"

msgid "remove contact"
msgstr "fjern kontakt"

msgid "change search mode"
msgstr "skift søgetilstand"

msgid "close contact details panel"
msgstr "luk panel for kontaktdetaljer"

msgid "Enter"
msgstr "Retur"

msgid "open note"
msgstr "åbn note"

msgid "add note"
msgstr "tilføj note"

msgid "remove note"
msgstr "fjern note"

msgid "edit note name and category"
msgstr "rediger notenavn og -kategori"

msgid "close note editor"
msgstr "luk noteredigeringsprogram"

msgid "save note"
msgstr "gem note"

msgid "find text"
msgstr "find tekst"

msgid "clear selection attributes"
msgstr "fjern valgte attributter"

msgid "open URLs"
msgstr "åbn adresser"

msgid "show document statistics"
msgstr "vis dokumentstatistik"

msgid ""
"OSMO was designed keeping in mind the user convenience, so there are many "
"key shortcuts. Here is the full list:"
msgstr ""
"OSMO blev designet med henblik på brugeranvendelighed, derfor er der mange "
"genvejstaster. Her er hele listen:"

msgid "Note editor"
msgstr "Noteredigeringsprogram"

msgid "Selector"
msgstr "Vælger"

msgid "Editor"
msgstr "Redigeringsprogram"

msgid "A handy personal organizer"
msgstr "En nem personlig planlægger"

msgid "compiled on"
msgstr "kompileret den"

msgid "Programming"
msgstr "Programmering"

msgid "Graphics"
msgstr "Grafik"

msgid "Contributors"
msgstr "Bidragydere"

msgid "Translators"
msgstr "Oversættere"

msgid "Mailing lists"
msgstr "Postliste"

msgid "Bug tracker"
msgstr "Fejlregistrering"

msgid "Feature requests"
msgstr "Funktionsønsker"

msgid "Available modules"
msgstr "Tilgængelige moduler"

msgid "Compiled-in features"
msgstr "Kompilerede funktioner"

msgid "iCalendar support"
msgstr "Understøttelse af iCalendar"

msgid "Encrypted notes support"
msgstr "Understøttelse af krypterede noter"

msgid "Backup support"
msgstr "Understøttelse af sikkerhedskopiering"

msgid "Printing support"
msgstr "Understøttelse af udskrivning"

msgid "Spell checker support"
msgstr "Understøttelse af stavekontrol"

msgid "version"
msgstr "version"

msgid "GIT version"
msgstr "GIT-version"

msgid "Key shortcuts"
msgstr "Genvejstast"

msgid "License"
msgstr "Licens"

#~ msgid "handy personal organizer"
#~ msgstr "nem personlig planlægger"
